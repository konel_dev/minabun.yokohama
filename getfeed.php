<?php 

//tokenを取得
$auth = dirname(__FILE__)."/auth.json";
$json = file_get_contents($auth);
$jsonArray = json_decode($json,true);
$access_token = $jsonArray["access_token"];

$graph_path = array(
    'uri' => "https://graph.facebook.com",
    'version'  => "v8.0",
    'account'  => "minabun.yokohama",
    'endpoint' => "posts"
);

$graph_url = implode($graph_path,"/")."?access_token=".$access_token.'&fields=created_time,id,message,full_picture';
$options = [
    'https' => [
        'method'  => 'GET',
        'timeout' => 3, // タイムアウト時間
    ]
];

echo "========\n";
echo "Starting...\n";

$json= file_get_contents($graph_url,false, stream_context_create($options));

// もしFalseが返っていたらエラーなので空白配列を返す
if ($json === false) {
    echo "Error:JSON empty.\n";
    exit();
}

// 200以外のステータスコードは失敗とみなし空配列を返す
preg_match('/HTTP\/1\.[0|1|x] ([0-9]{3})/', $http_response_header[0], $matches);
$statusCode = (int)$matches[1];
if ($statusCode !== 200) {
    echo "Error: Got status".$statusCode.".\n";
    exit();
}

// 文字列から変換
$jsonArray = json_decode($json, true);
$jsonArray['feed_at'] = date("Y-m-d H:i:s");

$json = fopen(dirname(__FILE__).'/public/feed/feed.json', 'w+b');
fwrite($json, json_encode($jsonArray));
fclose($json);

echo "Get feed done\n";
echo "========\n";