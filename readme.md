# みっけるみなぶん サイト仕様書

## ディレクトリ構成

```
├── auth.json                 // Facebook 認証Token保存ファイル
├── getfeed.php               // FacebookFeed取得スクリプト
├── gettoken.php              // Facebook 認証Token取得スクリプト
├── public                    // 公開ディレクトリ
│   ├── apple-touch-icon.png  ──// Facebook 認証Token取得スクリプト
│   ├── favicon.ico           ──// Favicon（アイコン）
│   ├── favicon.png           ──// Favicon（PNG）
│   ├── feed                  
│   │   └── feed.json         ────// FacebookFeed保存ファイル
│   ├── images                
│   │   └── ...               ────// 各種ファイル
│   ├── index.php             ──// TOPページのhtmlファイル
│   ├── js                    
│   │   └── functions.js      ────// javaScriptファイル
│   ├── robots.txt            ──// 検索エンジン設定ファイル
│   └── style.css             ──// スタイルシート
└── readme.md                 // 本ドキュメント
```

## サイト設置に際して

php 5.4.45 / 7.1.x では動作確認済み。
公開ディレクトリはpublicになるため、サーバー上に設置する際にはドキュメントルートを `public` に設定する。

## FacebookFeedに関して

本サイトではFacebookページのフィードを取得し、サイト内に記事として表示されるような仕組みを用いている。

### 関連ファイル

- gettoken.php - Facebookフィードを取得するためのTokenを更新するスクリプト  
- getfeed.php - Facebookフィードを取得するスクリプト。取得したfeedはjsonとして `public/feed/feed.json` に保存される  
- auth.json - Facebookフィードを取得するためのTokenをjson方式で保存するjson。初期は開発者が設定し、その後はgettokenを実行すると自動更新される  
- public/feed/feed.json - FacebookFeedのデータをjsonで保存しているファイル  

### 設置手順

FacebookFeedを取得するために、以下の手順で設置する。

1. FacebookDeveloperダッシュボードで、アプリID `822768625151712` のTokenを取得する（60日間実行されてない場合はTokenがExpireしているので再取得するがある）
1. auth.jsonにtokenを保存する `{"access_token":"YourTokenHere"}`
1. Cronの設定
  - FacebookFeedを2分に1回取得するためのCronを設定する  
  `*/2 * * * * php /path/to/minabun.yokohama/getfeed.php`  

  - 認証用Tokenを5日1回更新する
  `0 0 */5 * * php /path/to/minabun.yokohama/gettoken.php`  

## ローカル開発手順

1. プロジェクトフォルダまで移動
1. `php -S localhost:8000`
1. `http://localhost:8000/public/` へアクセス