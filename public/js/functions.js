
jQuery( function( $ ) {
	
	
	/**
	 * Functions
	 */
	
	var
	body = $( 'body' ),
	drawer = $( '#burger' ),
	Status = {
		CLOSE: 0,
		OPEN: 1
	},
	status = Status.CLOSE;
	drawer.bind( 'touchstart click', function() {
		switch( status ) {
			case Status.CLOSE:
				status = Status.OPEN;
				body.addClass( 'menu-open' );
				break;
			case Status.OPEN:
				status = Status.CLOSE;
				body.removeClass( 'menu-open' );
				break;
		}
		return false;
	} );
	
	// Smooth scroll
	$( 'header nav a' ).click( function() {
		var href = $( this ).attr( 'href' );
		var target = $( href === '#' || href === '' ? 'html' : href );
		if ( target.get( 0 ) ) {
			status = Status.CLOSE;
			body.removeClass( 'menu-open' );
			setTimeout( function() {
				$( 'html, body' ).stop().animate( {
					scrollTop: target.offset().top - 50
				}, 500, 'swing' );
			}, 400 );
		}
		return false;
	} );
	$( '#to_top' ).click( function() {
		var href = $( this ).attr( 'href' );
		var target = $( href === '#' || href === '' ? 'html' : href );
		if ( target.get( 0 ) ) {
			$( 'html, body' ).stop().animate( {
				scrollTop: target.offset().top
			}, 500, 'swing' );
		}
		return false;
	} );
	
	// Grid set
	function gridSet( gridItem ) {
		if ( gridItem.get( 0 ) ) {
			
			var baseHeight = 0;
			gridItem.height( "" ).each( function() {
				if ( baseHeight < $( this ).height() ) {
					baseHeight = $( this ).height();
				}
			} ).height( baseHeight );
			
		}
	}
	gridSet( $( '#front-1 li .post-content' ) );
	$( window ).resize( function() {
		gridSet( $( '#front-1 li .post-content' ) );
	} );
	
} );
