<!DOCTYPE html>
<html lang="ja">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta charset="UTF-8">
<title>みなぶん</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<meta name="format-detection" content="telephone=no, address=no, email=no">
<meta property="og:locale" content="ja_JP">
<meta property="og:site_name" content="みなぶん">
<meta property="og:type" content="website">
<meta property="og:title" content="みなぶん">
<meta property="og:description" content="">
<meta property="og:url" content="">
<meta property="og:image" content="images/og_image.png">
<meta name="twitter:card" content="summary">
<meta name="twitter:image:src" content="">
<link rel="icon" href="favicon.ico">
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;500;700&family=Yantramanav:wght@400;500;700&display=swap">
<link rel="stylesheet" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/functions.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-D0K37ZSWWP"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'G-D0K37ZSWWP');
</script>

</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.6";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

    <div id="viewport">
        <div id="container">
            <header>
				<button id="burger">
					<p><span></span><span></span><span></span></p>
				</button>
				<nav>
					<div class="inside">
						<h1 id="logo"><img src="images/logo.svg" alt="みっけるみなぶん"></h1>
						<ul>
							<li><a href="#mainview"><span class="content-number">0.</span> T O P </a></li>
							<li><a href="#new"><span class="content-number">1.</span> 新着情報</a></li>
							<li><a href="#front-2"><span class="content-number">2.</span> 「みっけるみなぶん」とは？</a></li>
							<li><a href="#front-3"><span class="content-number">3.</span> みっけるみなぶんマップ</a></li>
							<li><a href="#front-4"><span class="content-number">4.</span> “みなぶんでっき”を使ってみよう！</a></li>
							<li><a href="#access_map"><span class="content-number">5.</span> アクセス</a></li>
							<li><a href="#contact"><span class="content-number">6.</span> お問い合わせ</a></li>
						</ul>
					</div>
				</nav>
            </header>
			<main>
				<div id="mainview">
					<div class="inside">
						<h1><img src="images/logo.svg" alt="みっけるみなぶん"></h1>
						<p id="discover"><span>Discover your place!</span><br><span><img src="images/text-discover.svg" alt="あなたの場所をみっけよう！"></span></p>
						<p id="date"><span>2020.</span><br><span>11.9<small>(MON)</small> - 11.30<small>(MON)</small></span></p>
					</div>
				</div>
				<div id="content">
					<section id="front-1">
						<div class="inside">
							<p id="first_message">「みっけるみなぶん」は、<br class="sp">みちの新しい使い方を見つける社会実験です。<br>
								さあ、あなたの場所をみっけてください！</p>
							<div class="section-title" id="new">
								<h1><span class="section-title_number">1.</span><br><img src="images/title-front-1.svg" alt="新着情報"></h1>
							</div>

							<?php 

							$url = "https://minabun.yokohama/feed/feed.json";

							$json = file_get_contents($url);
							$arr = json_decode($json);
							if ( $arr ) :
								$fb_url = "https://www.facebook.com/";

								#関数 : 文中の¥nを<br>に、リンクを<a>に
								function br_and_url_to_a($text){
									$pattern = '/((?:https?|ftp):\/\/[-_.!~*\'()a-zA-Z0-9;\/?:@&=+$,%#]+)/';
									$replace = '<a href="$1" target="_blank">$1</a>';
									$string = preg_replace( $pattern, $replace, $text );
									return nl2br($string);
								} ?>
								<ul>
									<?php
									$key=0;
									foreach($arr->data as $a):
										if ($key>=3) break;
										if(isset($a->message)):
											$key++;
											# Timezone修正
											$date = date("Y-m-d H:i:s", strtotime($a->created_time));
											$dt = DateTime::createFromFormat('Y-m-d H:i:s', $date, new DateTimeZone('America/Los_Angeles'));
											$dt = $dt->setTimeZone(new DateTimeZone('Asia/Tokyo')); ?>
											<li>
												<a href="<?php echo $fb_url.$a->id; ?>" target="_blank">
													<div class="post-header">
														<time><?php echo $dt->format('Y-m-d'); ?></time>
													</div>
													<div class="thumbnail" style="background-image: url(<?php if(isset($a->full_picture)) echo $a->full_picture; ?>);"></div>
													<div class="post-content">
														<?php
														$text = $a->message;
														$limit = 60;
														if(mb_strlen($text) > $limit) $text = mb_substr($text, 0, $limit); ?>
														<p><?php echo br_and_url_to_a($text); ?></p>
														<p class="to_link">詳しくはこちら</p>
													</div>
												</a>
											</li>
										<?php endif;
									endforeach; ?>
								</ul>
							<?php endif; ?>
						</div>
					</section>
					<section id="front-2">
						<div class="inside">
							<div class="section-title">
								<h1><span class="section-title_number">2.</span><br><img src="images/title-front-2.svg" alt="「みっけるみなぶん」とは？"><img src="images/title-front-2-white.svg" alt="「みっけるみなぶん」とは？" class="white"></h1>
							</div>
							<div class="section-body">
								<img src="images/front-2-image.jpg" id="front-2-image">
								<p>「みなぶん」では、<br>
									地域の回遊性の向上、<br>
									賑わいや魅力創出を目指し、<br>
									車道幅を狭め歩道を広げる等の<br>
									再整備を予定しています。<br>
									「みっけるみなぶん」では、<br>
									将来の道路を想像した議論や<br>
									実験をしていきたいと考えています。<br>
									実験中、いくつか異なるタイプの<br>
									スペースを設けます。<br>
									みなぶんを訪れ、<br>
									あなたの場所をみっけてください！<br>
									<br>
									<small>※みなぶんとは、再整備が計画されている<br>
									「みなと大通り及び横浜文化体育館周辺道路」の略称です</small></p>
							</div>
						</div>
					</section>
					<section id="front-3">
						<div class="inside">
							<div class="section-title">
								<h1><span class="section-title_number">3.</span><br><img src="images/title-front-3.svg" alt="みっけるみなぶんMAP"></h1>
							</div>
							<div id="minabun_map">
								<img src="images/minabun_map.png" alt="みっけるみなぶんMAP">
							</div>
							<ul>
								<li>
									<div class="list-content">
										<h1><img src="images/front-3-list_title-1.svg" alt="みなぶんでっき１"><img src="images/front-3-list_title-1-sp.svg" alt="みなぶんでっき１" class="sp"></h1>
										<p>近くにコンビニや公園があります。<br>デッキにイスとテーブルを<br>ご用意しました。</p>
									</div>
									<div class="list-image">
										<img src="images/front-3-list_image-1.png" alt="みなぶんでっき１イメージ">
									</div>
								</li>
								<li>
									<div class="list-content">
										<h1><img src="images/front-3-list_title-2.svg" alt="みなぶんでっき２"><img src="images/front-3-list_title-2-sp.svg" alt="みなぶんでっき２" class="sp"></h1>
										<p>居酒屋やバルの目の前。<br>デッキ、イス、テーブルやカウンターで<br>お待ちしております。</p>
									</div>
									<div class="list-image">
										<img src="images/front-3-list_image-2.png" alt="みなぶんでっき２イメージ">
									</div>
								</li>
								<li>
									<div class="list-content">
										<h1><img src="images/front-3-list_title-3.svg" alt="みなぶんでっき３"><img src="images/front-3-list_title-3-sp.svg" alt="みなぶんでっき３" class="sp"></h1>
										<p>カフェの集まる立地に、人工芝やデッキ、木の格子の壁、<br>様々な家具で、3つの選べるスペースをつくりました。</p>
									</div>
									<div class="list-image">
										<img src="images/front-3-list_image-3.png" alt="みなぶんでっき３イメージ">
									</div>
								</li>
								<li>
									<div class="list-content">
										<h1><img src="images/front-3-list_title-4.svg" alt="みなぶんでっき４"><img src="images/front-3-list_title-4-sp.svg" alt="みなぶんでっき４" class="sp"></h1>
										<p>開港記念会館や岡倉天心生誕碑を<br>眺めてゆったりできる、<br>歴史を感じるスペースです。</p>
									</div>
									<div class="list-image">
										<img src="images/front-3-list_image-4.png" alt="みなぶんでっき４イメージ">
									</div>
								</li>
							</ul>
						</div>
					</section>
					<section id="front-4">
						<div class="inside">
							<div class="section-title">
								<h1><span class="section-title_number">4.</span><br><img src="images/title-front-4.svg" alt="“みなぶんでっき”を使ってみよう ！"></h1>
								<p>「みっけるみなぶん」は<br class="sp">みちの新しい使い方を見つける実験のこと。<br>さぁ、みなぶんであなたも自分の場所を<br class="sp">みっけよう！</p>
							</div>
							<ul>
								<li>
									<p>すわる・やすむ</p>
									<div class="list-image">
										<img src="images/front-4-list_image-1.svg" alt="すわる・やすむ">
									</div>
								</li>
								<li>
									<p>飲食する</p>
									<div class="list-image">
										<img src="images/front-4-list_image-2.svg" alt="飲食する">
									</div>
								</li>
								<li>
									<p>お話しする</p>
									<div class="list-image">
										<img src="images/front-4-list_image-3.svg" alt="お話しする">
									</div>
								</li>
								<li>
									<p>待ち合わせする</p>
									<div class="list-image">
										<img src="images/front-4-list_image-4.svg" alt="待ち合わせする">
									</div>
								</li>
								<li>
									<p class="sp">本を読む</p>
									<div class="list-image">
										<img src="images/front-4-list_image-5.svg" alt="本を読む">
									</div>
									<p>本を読む</p>
								</li>
								<li>
									<p class="sp">テイクアウト／<br>デリバリーする</p>
									<div class="list-image">
										<img src="images/front-4-list_image-6.svg" alt="テイクアウト／デリバリーする">
									</div>
									<p>テイクアウト／<br>デリバリーする</p>
								</li>
								<li>
									<div class="list-image">
										<img src="images/front-4-list_image-7.svg" alt="他にもみっけよう！">
									</div>
									<p>他にもみっけよう！</p>
								</li>
							</ul>
						</div>
					</section>
					<section id="usage_rules">
						<div class="inside">
							<div class="section-title">
								<h2><img src="images/title-usage_rules.svg" alt="ご利用ルール"></h2>
							</div>
							<ul>
								<li>・ ⾃由にご利⽤ください（⾃分なりの使い⽅をみっけよう）</li>
								<li>・ ⽕気厳禁・禁煙</li>
								<li>・ ごみは持ち帰りましょう（ごみ箱はありません）</li>
								<li>・ 場所は譲り合いましょう</li>
								<li>・ 距離をとって感染対策</li>
								<li>・ 夜は静かに、周囲の迷惑になることはやめましょう</li>
								<li>・ 販売⾏為はできません</li>
							</ul>
							<div id="about_infection_control">
								<h3><img src="images/title-about_infection_control.svg" alt="新型コロナウイルス感染拡大防止対策について"></h3>
								<p>・発熱等の風邪の症状がある方、体調がすぐれない方は、ご来場をお控えください。</p>
								<p>・来場にあたり、マスクの着用、手指の消毒、来場者同士の距離の確保へのご協力をお願いいたします。</p>
							</div>
							<div id="access_map">
								<div class="section-title">
									<h2><img src="images/title-access_map.svg" alt="アクセスMAP"></h2>
								</div>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3250.376508583481!2d139.63795021525232!3d35.44547218024996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzXCsDI2JzQzLjciTiAxMznCsDM4JzI0LjUiRQ!5e0!3m2!1sja!2sjp!4v1604574486392!5m2!1sja!2sjp" width="100%" height="" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
							</div>
						</div>
					</section>
				</div>
			</main>
			<footer>
				<div class="inside">
					<h1 id="footer-logo"><img src="images/logo.svg" alt="みっけるみなぶん"></h1>
					<div id="contact">
						<p>主催 ： 横浜市<br>
							実施・運営 ： 日建設計シビル・カナコン・オンデザインパートナーズJV<br>
							実験に関する内容のお問い合わせはこちらまで<br>
							MAIL : <a href="mailto: info＠minabun.yokohama">info＠minabun.yokohama</a><br>
							<a href="https://www.city.yokohama.lg.jp/kurashi/machizukuri-kankyo/doro/jigyo_kikaku/mina-bun.html" target="_blank">みなぶん再整備事業に関する詳細はこちら  >>></a><br>
							［ アンケート実施中！ ］<br>
							本社会実験の結果を今後の整備に活かすため、<br>
							<a href="https://docs.google.com/forms/d/e/1FAIpQLSfK0sBF2EgbtVcfdQNRvQ16ijSoM7pbl3eLp5D13PO_AxhirQ/viewform" target="_blank">アンケートへのご協力をお願いいたします！  >>></a>
					</div>
					<a href="#" id="to_top"><img src="images/to_top.png" alt="TOP"></a>
				</div>
			</footer>
		</div>
    </div><!-- #viewport -->
	
</body>
</html>