<?php 

//tokenを取得
$auth = dirname(__FILE__)."/auth.json";
$json = file_get_contents($auth);
$jsonArray = json_decode($json,true);
$access_token = $jsonArray["access_token"];

date_default_timezone_set('Asia/Tokyo');

$url = "https://graph.facebook.com/v8.0/oauth/access_token?grant_type=fb_exchange_token&client_id=822768625151712&client_secret=5f59a0c63a5950ba796f52afa219cfab&fb_exchange_token=";

$options = [
    'https' => [
        'method'  => 'GET',
        'timeout' => 3, // タイムアウト時間
    ]
];

echo "========\n";
echo "Exchanging Token...\n";

$json = file_get_contents($url.$access_token, false, stream_context_create($options));

$jsonArray = json_decode($json, true);
$jsonArray['auth_at'] = date("Y-m-d H:i:s");

$json = fopen(dirname(__FILE__).'/auth.json', 'w+b');
fwrite($json, json_encode($jsonArray));
fclose($json);

//echo json_encode($jsonArray);
echo "Get Token done\n";
echo "========\n";